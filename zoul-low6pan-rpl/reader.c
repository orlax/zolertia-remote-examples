#include "contiki.h"
#include "dev/adc-sensors.h"
#include "dev/zoul-sensors.h"
#include "cpu.h"
#include "sys/process.h"
#include "dev/sys-ctrl.h"
#include "lib/list.h"
#include "power-mgmt.h" 
#include "dev/adc-zoul.h"
#include "dev/leds.h"
#include <stdio.h> 
#include "dht22.h"
#include "net/routing/routing.h"
#include "random.h"
#include "net/netstack.h"
#include "net/ipv6/simple-udp.h"

#include "sys/log.h"
#define LOG_MODULE "App"
#define LOG_LEVEL LOG_LEVEL_INFO

#define WITH_SERVER_REPLY  1
#define UDP_CLIENT_PORT	8765
#define UDP_REPEATER_PORT 8766
#define UDP_SERVER_PORT	5678

#define START_INTERVAL		(15 * CLOCK_SECOND)
#define SEND_INTERVAL		  (60 * CLOCK_SECOND)

static struct simple_udp_connection udp_conn;

#define TEST_LEDS_FAIL                    leds_off(LEDS_ALL); \
                                           leds_on(LEDS_RED);  \
                                          PROCESS_EXIT();

#define LOOPINTERVAL (CLOCK_SECOND * 2)


//callback de comunicacion
/*---------------------------------------------------------------------------*/
static void
udp_rx_callback(struct simple_udp_connection *c,
         const uip_ipaddr_t *sender_addr,
         uint16_t sender_port,
         const uip_ipaddr_t *receiver_addr,
         uint16_t receiver_port,
         const uint8_t *data,
         uint16_t datalen)
{
  unsigned count = *(unsigned *)data;
  LOG_INFO("Received response %u from ", count);
  LOG_INFO_6ADDR(sender_addr);
  LOG_INFO_("\n");
}
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
PROCESS(reader_and_sender_process, "Reader and sender");
AUTOSTART_PROCESSES(&reader_and_sender_process);
/*---------------------------------------------------------------------------*/
static struct etimer et; // objeto para el timer 
uip_ipaddr_t dest_ipaddr;

PROCESS_THREAD(reader_and_sender_process, ev, data)
{
  static int ciclo;
  static int cpu_temp; 
  static int16_t dth22_temp, dth22_hum, soil; 

  PROCESS_BEGIN();

  /* Initialize UDP connection */
  simple_udp_register(&udp_conn, UDP_CLIENT_PORT, NULL,
                      UDP_SERVER_PORT, udp_rx_callback);

  ciclo = 0; 

  /* Configure ADC channel for soil moisture measurements */
  adc_zoul.configure(SENSORS_HW_INIT, ZOUL_SENSORS_ADC_ALL);
  SENSORS_ACTIVATE(dht22); //activamos el sensor dht22

  printf("Iniciando programa de recoleccion y envio de datos.\n");
  printf("v 0.01.\n");

  while(1){
      etimer_set(&et,LOOPINTERVAL);//seteamos el timer para cada cinco segundos. 
      
      
      PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et)); //aqui esperamos a que el timer se dispare. 
      leds_off(LEDS_ALL);
      //soil value. 
      soil = adc_zoul.value(ZOUL_SENSORS_ADC3);
      printf("soil: %d ", soil);
      
      //leemos los datos del sensor y los imprimos. 
      if(dht22_read_all(&dth22_temp, &dth22_hum) != DHT22_ERROR) {
      printf("dht22 temp: %02d.%02d ºC, ", dth22_temp / 10, dth22_temp % 10);
      printf("dht22_hum: %02d.%02d RH, ", dth22_hum / 10, dth22_hum % 10);
        } else {
      printf("No se pudo leer el sensor dht22, ");
      }

      //cpu temp 
      cpu_temp = cc2538_temp_sensor.value(CC2538_SENSORS_VALUE_TYPE_CONVERTED);
      ciclo = ciclo+1;

      printf("cpu temp: %02d , ciclo: %d\n", cpu_temp, ciclo); 

      if(NETSTACK_ROUTING.node_is_reachable() && NETSTACK_ROUTING.get_root_ipaddr(&dest_ipaddr)) {
      
      char payload_[50];
      sprintf(payload_, "%d,%d,%d,%d,%d", dth22_temp, dth22_hum, soil, cpu_temp, ciclo);

      /* Send to DAG root */      
      LOG_INFO("Sending request %s to ", payload_);
      LOG_INFO_6ADDR(&dest_ipaddr);
      LOG_INFO_("\n");
      leds_on(LEDS_BLUE);
      simple_udp_sendto(&udp_conn, &payload_, sizeof(payload_), &dest_ipaddr);
      
      ciclo++;
    } else {
      LOG_INFO("Not reachable yet\n");
      leds_on(LEDS_RED);
    }



  }

  
  
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
