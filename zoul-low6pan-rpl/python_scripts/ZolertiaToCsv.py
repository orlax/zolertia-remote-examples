# Este archivo se instala en la Raspberry Pi o en lla portatil
import csv
import serial
from datetime import datetime
import os
import yaml
import time
import os.path
import serial.tools.list_ports

# Wait for 5 seconds
time.sleep(5)

serialportName = "asdfasdf"
puertosSeriales = list(serial.tools.list_ports.comports())

for puerto in puertosSeriales:
    print puerto
    if "Zolertia" in puerto[1]:
        print "La conexion se realizara con el puerto: "+puerto[0]
        serialportName = puerto[0]



serialport = serial.Serial(serialportName,115200,timeout=10)

currentFile = "example3.csv"

maxLines = 3

path = "/home/orlando/Documentos/workspace/contiki-ng/examples/alejandro_camas/python_scripts"


def getNextFile():

    #esta linea revisa cada elemento dentro del directorio actual, y lo carga a un array si ese elemento es un archivo.
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    maxNumberFile = 0
    for f in files:
        #consigue el nombre del archivo antes del punto
        currentFilenumber = f.split(".")[0]
        currentFilenumber = yaml.load(currentFilenumber)
        #revisa que no sea el archivo .py y compara con el numero maximo actual
        if currentFilenumber != "ZolertiaToCsv" and currentFilenumber > maxNumberFile: 
	        maxNumberFile = currentFilenumber

    currentFile = str(maxNumberFile+1)+".csv"
    currentFile = os.path.join(path,currentFile)
  #os.path.join(config_root, sys.argv[1])
        # do something
    writeToFile(currentFile)


def writeToFile(currentFile):
    
    #abriendo archivo por primera vez
    with open(currentFile, 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',', quotechar = '|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['DHT22 Temp', 'DHT22 Hum', 'Soil','CPU Temp', 'Timestamp', 'Ciclo'])

        currentLine = 0
        while currentLine < maxLines:
            read = str(serialport.readline())
            data = read.split(",") #Genera un array con datos divididos por la coma
        
            if(len(data)>4):
                dht22_temp = float(data[0])/10
                dht22_hum  = float(data[1])/10
                soil = float(data[2])/10
                cpu_temp = float(data[3])/1000
                ciclo = int(data[4])
                time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                print "dht22 temp: "+str(dht22_temp)+" dht22 hum: "+str(dht22_hum)+" soil: "+ str(soil) +" cpu temp: "+ str(cpu_temp) + " "+ time +" ciclo: "+str(ciclo)
              
                filewriter.writerow([str(dht22_temp),str(dht22_hum),str(soil),str(cpu_temp),time,str(ciclo)])
              
                currentLine += 1
            else: 
                print "the readed data had a length less than 3"     

        csvfile.close()
        print "closing document and starting new"
        getNextFile()

getNextFile()


