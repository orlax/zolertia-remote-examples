import csv 
from datetime import datetime
import os 
import time 
import os.path
import yaml


maxLines = 10
currentFile = "1.csv"
step = 0

def getNextFile():
    #esta linea revisa cada elemento dentro del directorio actual, y lo carga a un array si ese elemento es un archivo.
    files = [f for f in os.listdir(os.path.dirname(os.path.abspath(__file__))) if os.path.isfile(f)]
    maxNumberFile = 0
    for f in files:
        #consigue el nombre del archivo antes del punto
        currentFilenumber = f.split(".")[0]
    	currentFilenumber = yaml.load(currentFilenumber)
        #revisa que no sea el archivo .py y compara con el numero maximo actual
        if currentFilenumber != "dummyCSV" and currentFilenumber > maxNumberFile: 
	        maxNumberFile = currentFilenumber

    currentFile = str(maxNumberFile+1)+".csv"
    currentFile = os.path.join(os.path.dirname(os.path.abspath(__file__)),currentFile)
    writeToFile(currentFile)


def writeToFile(currentFile):   
    global step
    #abriendo archivo por primera vez
    with open(currentFile, 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',', quotechar = '|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['step', 'timestamp'])
        currentLine = 0
        while currentLine < maxLines:
            print "writing to " + csvfile.name + " : "+str(step)+" at: "+ str(datetime.now())
            filewriter.writerow([step,datetime.now()])
            currentLine += 1
            step += 1
            time.sleep(3)
        csvfile.close()
        getNextFile()

getNextFile()
