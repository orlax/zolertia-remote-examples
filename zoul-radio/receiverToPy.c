#include "contiki.h"
#include "net/rime/rime.h"
#include "dev/leds.h"
#include "dev/button-sensor.h"
#include <stdio.h>
#include <string.h>

PROCESS(receiverProcess, "Recibidor de mensajes e impresor por serial"); 
AUTOSTART_PROCESSES(&receiverProcess); 

static void 
ManejadorDeMensajes(struct broadcast_conn *c, const linkaddr_t *sender){

    /* 
        el mensaje recibido tiene 4 valores separados por comas
        ciclo, cpu_temp, temperature, humidity. 
        vamos a iterar por ese texto y agregar cada valos a su variable respectiva
    */ 

    /* tomar el valor del mensaje recibido y mandarlo por serial junto a los otros valores */
    char *msg =  (char *)packetbuf_dataptr(); 
    char *coma = ","; 

    /* variables para los cuatro valores leidos */ 
    char *ciclo = "null"; 
    char *cpu_temp = "null";  
    char *temperature = "null";  
    char *humidity = "null"; 
    char *voltaje = "null"; 
    char *soil = "null"; 
    int8_t rssi = (int8_t)packetbuf_attr(PACKETBUF_ATTR_RSSI)/* consiguiendo el rssi */ 

    int currentValue = 0; 
    char *value = strtok(msg, coma); 

    while(value != NULL){
        if(currentValue==0) { ciclo = value;  }
        if(currentValue==1) { cpu_temp = value;  }
        if(currentValue==2) { temperature = value;  }
        if(currentValue==3) { humidity = value;  }
        if(currentValue==4) { voltaje = value; }
        if(currentValue==5) { soil = value; }
        value = strtok(NULL, coma); 
        currentValue = currentValue + 1;
    }

    printf("%s,%s,%s,%s,%d,%s,%s\n", ciclo, cpu_temp, temperature, humidity,rssi,voltaje,soil);
}


static struct broadcast_conn IRF;
static const struct broadcast_callbacks Receiver = {ManejadorDeMensajes}; 

PROCESS_THREAD(receiverProcess, ev, data){
    PROCESS_EXITHANDLER(broadcast_close(&IRF);)

    PROCESS_BEGIN(); 
        broadcast_open(&IRF, 129, &Receiver);


    PROCESS_END();  
}