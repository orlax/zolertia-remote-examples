/* 
    Programa de envio de de datos de prueba para el proyecto de investigacion de alejandro camas
    envia la temperatura y el ciclo actual en un mensaje en modo broadcast cada 5 segundos. 
*/

/* realizamos todos los includes necesarios para nuestro programa */ 
#include "contiki.h"
#include "dev/adc-zoul.h"
#include "dev/zoul-sensors.h"
#include "cpu.h"
#include "sys/process.h"
#include "net/rime/rime.h"
#include "dev/sys-ctrl.h"
#include "lib/list.h"
#include "power-mgmt.h" 
#include "dev/button-sensor.h"
#include "dev/dht22.h"
#include "dev/leds.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define TEST_LEDS_FAIL                    leds_off(LEDS_ALL); \
                                           leds_on(LEDS_RED);  \
                                          PROCESS_EXIT();

#define LOOPINTERVAL (CLOCK_SECOND * 2)


PROCESS(senderProcess, "Envio de datos"); 
AUTOSTART_PROCESSES(&senderProcess); 


static struct etimer timeup; 

static void ManejadorDeMensajes(struct broadcast_conn *c, const linkaddr_t *sender){
    printf("mensaje recibido\n"); 
}

static struct broadcast_conn IRF; 
static struct broadcast_callbacks Recibidor = {ManejadorDeMensajes}; 

PROCESS_THREAD(senderProcess, ev, data){

    // creamos las variables principales
    static int ciclo;
    static int cpu_temp; 
    static uint16_t voltaje, temperature, humidity, soil; 

    PROCESS_EXITHANDLER(broadcast_close(&IRF)); // si el programa se cierra, se cierra la interfaz de RF 
    PROCESS_BEGIN(); 

        ciclo = 0; 
        cpu_temp = 0; 
        temperature = 0; 
        humidity = 0; 
        soil = 0;

        /* activacion del modo de ahorro y seteo del timeout */
        if( pm_enable() != PM_SUCCESS){
            printf("ERROR: no funciona el modo de ahorro");
            TEST_LEDS_FAIL; 
        }
        printf("Modo de ahorro activado\n");  
        /* revision de la version de firmware?? no tocar */ 
        /*
        if((pm_get_fw_ver(0) == PM_ERROR) || (0 != PM_EXPECTED_VERSION)) {
        printf("PM: unexpected version 0x%02X\n", 0);
        TEST_LEDS_FAIL;
        }
        */
         printf("PM: firmware version 0x%02X OK\n", 0);

        /* revision de voltaje */ 
        if(pm_get_voltage(&voltaje) != PM_SUCCESS){
            printf("No se pudo leer el voltaje de la bateria\n"); 
            TEST_LEDS_FAIL;
        }
        printf("Voltaje (raw) = %u\n", voltaje);
 
        if(pm_set_timeout(PM_SOFT_SHTDN_5_7_SEC)!=PM_SUCCESS){
           printf("No se pudo setiar el timeout para el soft shutdown\n");
           TEST_LEDS_FAIL; 
        }
        printf("Timeout para apagado es: %lu\n", pm_get_timeout());

        /*  FIN DE LA CONFIGURACION DEL MODO DE AHORRO */

        leds_off(LEDS_ALL); 
        leds_on(LEDS_LIGHT_BLUE);

        broadcast_open(&IRF, 129, &Recibidor); /* abrimos el radio */
        adc_zoul.configure(SENSORS_HW_INIT, ZOUL_SENSORS_ADC_ALL); /* activamos los sensores */
                
        SENSORS_ACTIVATE(dht22);

        // obtenemos los valores para las lecturas
        cpu_temp = cc2538_temp_sensor.value(CC2538_SENSORS_VALUE_TYPE_CONVERTED); 
        ciclo = pm_get_num_cycles();

        //lectura sensor dth22
        if(dht22_read_all(&temperature, &humidity) != DHT22_ERROR){
            printf("Temperature and Humidity from dht22 sensor read ok"); 
        }else{
            printf("failed to read the sensors"); 
        }

         //soil value. 
        soil = adc_zoul.value(ZOUL_SENSORS_ADC3);
        printf("soil: %d ", soil);


        char msg[21]; // enough to hold all numbers up to 64-bits
        sprintf(msg, "%d,%d,%02d.%02d,%02d.%02d,%u,%d", ciclo, cpu_temp, temperature/10, temperature%10, humidity/10,humidity%10,voltaje,soil);

        packetbuf_copyfrom(msg, 22);
        broadcast_send(&IRF); 

        printf(" se envio el mensaje: %s\n", msg);

        etimer_set(&timeup, LOOPINTERVAL); 
        PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timeup)); 

        /* entrando en modo de apagado */ 
        if(pm_shutdown_now(PM_SOFT_SLEEP_CONFIG)==PM_SUCCESS){
            printf("Buenas noches!\n");
        }else{
            printf("Error al apagar\n");
            TEST_LEDS_FAIL;
        }

    PROCESS_END(); 
}