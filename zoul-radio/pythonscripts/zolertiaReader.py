# Este archivo se instala en la Raspberry Pi o en lla portatil
import csv
import serial
from datetime import datetime
import os
import yaml
import time
import os.path
import serial.tools.list_ports

# Wait for 5 seconds
time.sleep(3)

serialportName = "asdfasdf"
puertosSeriales = list(serial.tools.list_ports.comports())

for puerto in puertosSeriales:
    print puerto
    if "Zolertia" in puerto[1]:
        print "La conexion se realizara con el puerto: "+puerto[0]
        serialportName = puerto[0]



serialport = serial.Serial(serialportName,115200,timeout=10)

currentFile = "example3.csv"

maxLines = 10

path = "/home/pi/"


def getNextFile():

    #esta linea revisa cada elemento dentro del directorio actual, y lo carga a un array si ese elemento es un archivo.
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    maxNumberFile = 0
    for f in files:
        #consigue el nombre del archivo antes del punto
        currentFilenumber = f.split(".")[0]
        currentFilenumber = yaml.load(currentFilenumber)
        #revisa que no sea el archivo .py y compara con el numero maximo actual
        if currentFilenumber != "zolertiaReader" and currentFilenumber > maxNumberFile: 
	        maxNumberFile = currentFilenumber

    currentFile = str(maxNumberFile+1)+".csv"
    currentFile = os.path.join(path,currentFile)
  #os.path.join(config_root, sys.argv[1])
        # do something
    writeToFile(currentFile)


def writeToFile(currentFile):
    
    #abriendo archivo por primera vez
    with open(currentFile, 'wb') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',', quotechar = '|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['Ciclo', 'Temp CPU', 'Temp ambiente', 'Humidity ambiente','soil' ,'RSSI', 'Timestamp','Voltaje Bateria'])

        currentLine = 0
        while currentLine < maxLines:
            read = str(serialport.readline())
            data = read.split(",") #Genera un array con datos divididos por la coma
        
            if(len(data)>4):
                ciclo = data[0]
                cpu_temp  = int(data[1])/float(1000)
                temp = str(float(data[2])/10)
                hum = str(float(data[3])/10)
                rssi = int(data[4])
                voltaje = int(data[5])
                time = str(datetime.now()) 
                soil = float(data[6])/10
                print "ciclo: "+ciclo+" cpu_temp: "+str(cpu_temp)+" temp: "+ temp +" hum: "+ hum +" rssi: "+str(rssi)+" "+time + "  voltaje Bateria: "+voltaje + "soil: "+str(soil)
                filewriter.writerow([ciclo,str(cpu_temp),temp,hum,str(soil),str(rssi),time,str(voltaje)])
                currentLine += 1
            else: 
                print "the readed data had a length less than 3"

        csvfile.close()
        getNextFile()

getNextFile()


